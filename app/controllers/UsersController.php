<?php

class UsersController extends BaseController {

	public function index()
	{
		$records 			= User::orderBy('id_user', 'desc')->paginate(10);

		return View::make('admin/users/list')->with('records', $records);
	}

	public function usersView()
	{
		$records = User::find(Request::segment(4));

		return View::make('admin/users/view')->with('records', $records);
	}

	public function usersCreate()
	{
		$user_roles 				= UserRole::all();
		$departments				= Departments::all();
		$department_roles			= DepartmentRoles::all();

		return View::make('admin/users/create')
			->with('roles', $user_roles)
			->with('departments', $departments)
			->with('department_roles', $department_roles);
	}

	public function usersUpdate()
	{		
		$records 			= User::find(Request::segment(4));
		$user_roles 		= UserRole::all();
		$departments		= Departments::all();
		$department_roles	= DepartmentRoles::all();

		return View::make('admin/users/update')
			->with('records', $records)
			->with('roles', $user_roles)
			->with('departments', $departments)
			->with('department_roles', $department_roles);
	}

	public function usersDelete()
	{
		$records = User::find(Request::segment(4));

		return View::make('admin/users/delete')->with('records', $records);
	}

	public function usersDepartmentList()
	{
		$records 					= Departments::orderBy('id_department', 'desc')->paginate(10);
		$user_department_roles 		= DepartmentRoles::all();
		$department_users 			= DepartmentUsers::all();

		return View::make('admin/users/departments/list')
			->with('records', $records)
			->with('user_department_roles', $user_department_roles)
			->with('department_users', $department_users);
	}

	public function usersDepartmentCreate()
	{
		$records 	= Departments::all();
		$users 		= User::where('status', '=', '1')->get(); //PASS ONLY ACTIVE USERS TO THE VIEW

		return View::make('admin/users/departments/create')
			->with('records', $records)
			->with('users', $users);
	}

	public function usersDepartmentUpdate()
	{
		$records 	= Departments::find(Request::segment(5));
		$users 		= User::where('status', '=', '1')->get(); //PASS ONLY ACTIVE USERS TO THE VIEW

		return View::make('admin/users/departments/update')
			->with('records', $records)
			->with('users', $users);
	}

	public function usersDepartmentDelete()
	{
		$records = Departments::find(Request::segment(5));

		return View::make('admin/users/departments/delete')->with('records', $records);
	}

	public function usersDepartmentUsersAdd()
	{
		$department 		= Departments::find(Request::segment(5));
		$users 				= User::where('status', '=', '1')->get();
		$department_roles 	= DepartmentRoles::where('id_department', '=', Request::segment(5))->get();		

		return View::make('admin/users/departments/user_add')
			->with('department', $department)
			->with('users', $users)
			->with('department_roles', $department_roles);
	}

	public function usersDepartmentUsersEdit()
	{
		$users 				= User::find( Request::segment(5) );
		$departments 		= Departments::find( Request::segment(6));		
		$department_roles 	= DepartmentRoles::where('id_department', '=', Request::segment(6))->get();

		return View::make('admin/users/departments/user_edit')
			->with('departments', $departments)
			->with('users', $users)
			->with('department_roles', $department_roles);
	}

	public function usersDepartmentUsersDelete()
	{
		$users 				= User::find( Request::segment(5) );
		$departments 		= Departments::find( Request::segment(6));		
		$department_roles 	= DepartmentRoles::find(Request::segment(7));

		return View::make('admin/users/departments/user_delete')
			->with('departments', $departments)
			->with('users', $users)
			->with('department_roles', $department_roles);
	}			

	public function usersDepartmentRoleList()
	{		
		$user_roles = DepartmentRoles::orderBy('id_department', 'desc')->paginate(10);

		return View::make('admin/users/departments/role/list')->with('roles', $user_roles);
	}

	public function usersDepartmentRoleCreate()
	{
		$records 	= Departments::all();
		$users 		= User::orderBy('last_name', 'asc')->get();

		return View::make('admin/users/departments/role/create')
			->with('records', $records)
			->with('users', $users);
	}

	public function usersDepartmentRoleUpdate()
	{
		$records 	 = DepartmentRoles::find(Request::segment(6));
		$departments = Departments::all();

		foreach ($departments as $department) {
			$selected_dept = Departments::find( $department->id_department );
		}

		return View::make('admin/users/departments/role/update')
			->with('records', $records)
			->with('departments', $departments)
			->with('selected_dept', $selected_dept);
	}		

	public function usersRoleList()
	{
		$records = UserRole::all();

		return View::make('admin/users/role/list')->with('records', $records);
	}	

	public function usersRoleCreate()
	{
		return View::make('admin/users/role/create');
	}

	public function usersRoleUpdate()
	{
		$records = UserRole::find(Request::segment(5));

		return View::make('admin/users/role/update')->with('records', $records);
	}

	public function usersRoleDelete()
	{
		$records = UserRole::find(Request::segment(5));

		return View::make('admin/users/role/delete')->with('records', $records);
	}			

	public function passwordUpdate()
	{
		$records = User::find(Request::segment(5));

		return View::make('admin/users/update/password')->with('records', $records);
	}	
}