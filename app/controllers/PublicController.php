<?php

class PublicController extends BaseController {
	
	public function index()
	{
		return View::make('signin');
	}

	public function password_remind(){
		return View::make('password/remind');	
	}
}