<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users_leave', function(Blueprint $table)
		{
			$table->increments('id_leave');
			$table->integer('id_user')->unsigned();
			$table->foreign('id_user')->references('id_user')->on('users');		
			$table->string('first_name');
			$table->string('last_name');
			$table->string('leave_type');			
			$table->string('day_leave_type');
			$table->string('other_reason')->nullable();
			$table->string('leave_time')->nullable();
			$table->date('day_leave')->nullable();
			$table->date('date_from')->nullable();
			$table->date('date_to')->nullable();
			$table->decimal('number_of_days', 65,1);
			$table->text('leave_details');
			$table->integer('approval');
			$table->string('approved_by')->nullable();
			$table->date('approved_date')->nullable();
			$table->text('notes');
			$table->timestamps();			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users_leave');
	}

}