<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id_user');
			$table->integer('id_role')->unsigned();
			$table->foreign('id_role')->references('id_role')->on('users_role');
			$table->string('department_name');
			$table->string('department_role_name');
			$table->string('title');
			$table->string('first_name');
			$table->string('middle_name');			
			$table->string('last_name');
			$table->string('email');
			$table->string('password');
			$table->string('mobile');
			$table->string('tel');
			$table->string('street');			
			$table->string('town_city');
			$table->string('postal');
			$table->string('country');
			$table->string('company_name');	
			$table->text('job_scope');
			$table->date('employment_date');
			$table->date('employment_date_end');
			$table->string('salary');
			$table->string('bank');
			$table->string('bank_branch');
			$table->string('account_name');
			$table->string('account_number');
			$table->string('account_type');
			$table->text('others');
			$table->string('currency');
			$table->string('annual_leave');
			$table->string('sick_leave');
			$table->string('maternity_leave');
			$table->string('paternity_leave');
			$table->string('status');	
			$table->timestamps();			
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
