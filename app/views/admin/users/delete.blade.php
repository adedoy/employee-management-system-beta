@extends('admin.tpl.master')

@section('title')
  Users Delete - Employee Management and Leave System
@stop

@section('content')
	
	<div class="page-header">
		<h1>Users Delete</h1>
		<p>Confirm to delete this user.</p>
	</div>

	{{-- START THE DELETE SECTION --}}
	
	{{ Form::open() }}

		<div class="control-group well">
			<div class="controls">
				<p>Are you sure you want to delete: {{ $records->first_name }} {{ $records->last_name }}</p>
			</div>			
		</div>		

		<div class="control-group submit_button">
			<a href="{{ URL::to('admin/users/list') }}" class="btn btn-inverse">Cancel</a>
			<input type="submit" class="btn btn-primary input-xlarge" name="user_delete" id="user_delete" value="Delete this user" onClick="return confirm('Are you sure you want to delete this user?');">
		</div>
	{{ Form::close() }}	
@stop