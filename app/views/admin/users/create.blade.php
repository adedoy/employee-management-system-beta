@extends('admin.tpl.master')

@section('title')
  Users Create - Employee Management and Leave System
@stop

@section('content')

	<div class="page-header">
		<h1>Users Create</h1>
		<p>Create a user.</p>
	</div>

	{{-- START THE CREATE SECTION --}}
	
	{{ Form::open() }}

        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('first_name', '<li>:message</li>') }}
              {{ $errors->first('email', '<li>:message</li>') }}
              {{ $errors->first('password', '<li>:message</li>') }}
          </ul> 
        @endif

        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          	<p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif 
		
		<div class="control-group well pull-left span8">
			<div class="pull-left">
				<h3>Personal Information</h3> <hr />
				<div class="controls btn-group">
					<label>Your Title</label>
					<div class="btn">
						<label for="title_mr">Mr.</label> {{ Form::radio('title', 'Mr.', false, array('id' => 'title_mr') ) }}
					</div>
					<div class="btn">
						<label for="title_ms">Ms.</label> {{ Form::radio('title', 'Ms.', false, array('id' => 'title_ms') ) }}						
					</div>
					<div class="btn">
						<label for="title_mrs">Mrs.</label> {{ Form::radio('title', 'Mrs.', false, array('id' => 'title_mrs') ) }}
					</div>	
				</div>		
				<div class="controls">
					<label for="first_name">First Name <sup class="text-error">*</sup></label>					
					{{ Form::text('first_name', Input::old('first_name'), array('id' => 'first_name', 'class' => 'input-xlarge', 'placeholder' => 'First Name') ) }}
				</div>
				<div class="controls">
					<label for="middle_name">Middle Name</label>
					{{ Form::text('middle_name', Input::old('middle_name'), array('id' => 'middle_name', 'class' => 'input-xlarge', 'placeholder' => 'Middle Name') ) }}
				</div>
				<div class="controls">
					<label for="last_name">Last Name</label>
					{{ Form::text('last_name', Input::old('last_name'), array('id' => 'last_name', 'class' => 'input-xlarge', 'placeholder' => 'Last Name') ) }}
				</div>
				<div class="controls">
					<label for="email">Email <sup class="text-error">*</sup></label>
					{{ Form::text('email', Input::old('email'), array('id' => 'email', 'class' => 'input-xlarge', 'placeholder' => 'Email') ) }}
				</div>
				<div class="controls">
					<label for="password">Password <sup class="text-error">*</sup></label>
					{{ Form::password('password', array('id' => 'password', 'class' => 'input-xlarge', 'placeholder' => 'Password') ) }}
				</div>		
				<div class="controls">
					<label for="mobile">Mobile</label>
					{{ Form::text('mobile', Input::old('mobile'), array('id' => 'mobile', 'class' => 'input-xlarge', 'placeholder' => 'Mobile Phone') ) }}
				</div>
				<div class="controls">
					<label for="tel">Telephone</label>
					{{ Form::text('tel', Input::old('tel'), array('id' => 'tel', 'class' => 'input-xlarge', 'placeholder' => 'Telephone') ) }}
				</div>		
			</div>

			<div class="pull-right span4">
				<h3>Address</h3> <hr />
				<div class="controls">
					<label for="street">Street Address</label>
					{{ Form::text('street', Input::old('street'), array('id' => 'street', 'class' => 'input-xlarge', 'placeholder' => 'Street address') ) }}
				</div>
				<div class="controls">
					<label for="town_city">Town / City </label>
					{{ Form::text('town_city', Input::old('town_city'), array('id' => 'town_city', 'class' => 'input-xlarge', 'placeholder' => 'Town / City') ) }}					
				</div>
				<div class="controls">
					<label for="postal">Postal / Zip code</label>
					{{ Form::text('postal', Input::old('postal'), array('id' => 'postal', 'class' => 'input-xlarge', 'placeholder' => 'postal or zip code') ) }}
				</div>
				<div class="controls">
					<label for="country">Country</label>
					<select id="country" name="country">
						<option value="AFG">Afghanistan</option>
						<option value="ALA">Åland Islands</option>
						<option value="ALB">Albania</option>
						<option value="DZA">Algeria</option>
						<option value="ASM">American Samoa</option>
						<option value="AND">Andorra</option>
						<option value="AGO">Angola</option>
						<option value="AIA">Anguilla</option>
						<option value="ATA">Antarctica</option>
						<option value="ATG">Antigua and Barbuda</option>
						<option value="ARG">Argentina</option>
						<option value="ARM">Armenia</option>
						<option value="ABW">Aruba</option>
						<option value="AUS">Australia</option>
						<option value="AUT">Austria</option>
						<option value="AZE">Azerbaijan</option>
						<option value="BHS">Bahamas</option>
						<option value="BHR">Bahrain</option>
						<option value="BGD">Bangladesh</option>
						<option value="BRB">Barbados</option>
						<option value="BLR">Belarus</option>
						<option value="BEL">Belgium</option>
						<option value="BLZ">Belize</option>
						<option value="BEN">Benin</option>
						<option value="BMU">Bermuda</option>
						<option value="BTN">Bhutan</option>
						<option value="BOL">Bolivia, Plurinational State of</option>
						<option value="BES">Bonaire, Sint Eustatius and Saba</option>
						<option value="BIH">Bosnia and Herzegovina</option>
						<option value="BWA">Botswana</option>
						<option value="BVT">Bouvet Island</option>
						<option value="BRA">Brazil</option>
						<option value="IOT">British Indian Ocean Territory</option>
						<option value="BRN">Brunei Darussalam</option>
						<option value="BGR">Bulgaria</option>
						<option value="BFA">Burkina Faso</option>
						<option value="BDI">Burundi</option>
						<option value="KHM">Cambodia</option>
						<option value="CMR">Cameroon</option>
						<option value="CAN">Canada</option>
						<option value="CPV">Cape Verde</option>
						<option value="CYM">Cayman Islands</option>
						<option value="CAF">Central African Republic</option>
						<option value="TCD">Chad</option>
						<option value="CHL">Chile</option>
						<option value="CHN">China</option>
						<option value="CXR">Christmas Island</option>
						<option value="CCK">Cocos (Keeling) Islands</option>
						<option value="COL">Colombia</option>
						<option value="COM">Comoros</option>
						<option value="COG">Congo</option>
						<option value="COD">Congo, the Democratic Republic of the</option>
						<option value="COK">Cook Islands</option>
						<option value="CRI">Costa Rica</option>
						<option value="CIV">Côte d'Ivoire</option>
						<option value="HRV">Croatia</option>
						<option value="CUB">Cuba</option>
						<option value="CUW">Curaçao</option>
						<option value="CYP">Cyprus</option>
						<option value="CZE">Czech Republic</option>
						<option value="DNK">Denmark</option>
						<option value="DJI">Djibouti</option>
						<option value="DMA">Dominica</option>
						<option value="DOM">Dominican Republic</option>
						<option value="ECU">Ecuador</option>
						<option value="EGY">Egypt</option>
						<option value="SLV">El Salvador</option>
						<option value="GNQ">Equatorial Guinea</option>
						<option value="ERI">Eritrea</option>
						<option value="EST">Estonia</option>
						<option value="ETH">Ethiopia</option>
						<option value="FLK">Falkland Islands (Malvinas)</option>
						<option value="FRO">Faroe Islands</option>
						<option value="FJI">Fiji</option>
						<option value="FIN">Finland</option>
						<option value="FRA">France</option>
						<option value="GUF">French Guiana</option>
						<option value="PYF">French Polynesia</option>
						<option value="ATF">French Southern Territories</option>
						<option value="GAB">Gabon</option>
						<option value="GMB">Gambia</option>
						<option value="GEO">Georgia</option>
						<option value="DEU">Germany</option>
						<option value="GHA">Ghana</option>
						<option value="GIB">Gibraltar</option>
						<option value="GRC">Greece</option>
						<option value="GRL">Greenland</option>
						<option value="GRD">Grenada</option>
						<option value="GLP">Guadeloupe</option>
						<option value="GUM">Guam</option>
						<option value="GTM">Guatemala</option>
						<option value="GGY">Guernsey</option>
						<option value="GIN">Guinea</option>
						<option value="GNB">Guinea-Bissau</option>
						<option value="GUY">Guyana</option>
						<option value="HTI">Haiti</option>
						<option value="HMD">Heard Island and McDonald Islands</option>
						<option value="VAT">Holy See (Vatican City State)</option>
						<option value="HND">Honduras</option>
						<option value="HKG">Hong Kong</option>
						<option value="HUN">Hungary</option>
						<option value="ISL">Iceland</option>
						<option value="IND">India</option>
						<option value="IDN">Indonesia</option>
						<option value="IRN">Iran, Islamic Republic of</option>
						<option value="IRQ">Iraq</option>
						<option value="IRL">Ireland</option>
						<option value="IMN">Isle of Man</option>
						<option value="ISR">Israel</option>
						<option value="ITA">Italy</option>
						<option value="JAM">Jamaica</option>
						<option value="JPN">Japan</option>
						<option value="JEY">Jersey</option>
						<option value="JOR">Jordan</option>
						<option value="KAZ">Kazakhstan</option>
						<option value="KEN">Kenya</option>
						<option value="KIR">Kiribati</option>
						<option value="PRK">Korea, Democratic People's Republic of</option>
						<option value="KOR">Korea, Republic of</option>
						<option value="KWT">Kuwait</option>
						<option value="KGZ">Kyrgyzstan</option>
						<option value="LAO">Lao People's Democratic Republic</option>
						<option value="LVA">Latvia</option>
						<option value="LBN">Lebanon</option>
						<option value="LSO">Lesotho</option>
						<option value="LBR">Liberia</option>
						<option value="LBY">Libya</option>
						<option value="LIE">Liechtenstein</option>
						<option value="LTU">Lithuania</option>
						<option value="LUX">Luxembourg</option>
						<option value="MAC">Macao</option>
						<option value="MKD">Macedonia, the former Yugoslav Republic of</option>
						<option value="MDG">Madagascar</option>
						<option value="MWI">Malawi</option>
						<option value="MYS">Malaysia</option>
						<option value="MDV">Maldives</option>
						<option value="MLI">Mali</option>
						<option value="MLT">Malta</option>
						<option value="MHL">Marshall Islands</option>
						<option value="MTQ">Martinique</option>
						<option value="MRT">Mauritania</option>
						<option value="MUS">Mauritius</option>
						<option value="MYT">Mayotte</option>
						<option value="MEX">Mexico</option>
						<option value="FSM">Micronesia, Federated States of</option>
						<option value="MDA">Moldova, Republic of</option>
						<option value="MCO">Monaco</option>
						<option value="MNG">Mongolia</option>
						<option value="MNE">Montenegro</option>
						<option value="MSR">Montserrat</option>
						<option value="MAR">Morocco</option>
						<option value="MOZ">Mozambique</option>
						<option value="MMR">Myanmar</option>
						<option value="NAM">Namibia</option>
						<option value="NRU">Nauru</option>
						<option value="NPL">Nepal</option>
						<option value="NLD">Netherlands</option>
						<option value="NCL">New Caledonia</option>
						<option value="NZL">New Zealand</option>
						<option value="NIC">Nicaragua</option>
						<option value="NER">Niger</option>
						<option value="NGA">Nigeria</option>
						<option value="NIU">Niue</option>
						<option value="NFK">Norfolk Island</option>
						<option value="MNP">Northern Mariana Islands</option>
						<option value="NOR">Norway</option>
						<option value="OMN">Oman</option>
						<option value="PAK">Pakistan</option>
						<option value="PLW">Palau</option>
						<option value="PSE">Palestinian Territory, Occupied</option>
						<option value="PAN">Panama</option>
						<option value="PNG">Papua New Guinea</option>
						<option value="PRY">Paraguay</option>
						<option value="PER">Peru</option>
						<option value="Philippines" selected="selected">Philippines</option>
						<option value="PCN">Pitcairn</option>
						<option value="POL">Poland</option>
						<option value="PRT">Portugal</option>
						<option value="PRI">Puerto Rico</option>
						<option value="QAT">Qatar</option>
						<option value="REU">Réunion</option>
						<option value="ROU">Romania</option>
						<option value="RUS">Russian Federation</option>
						<option value="RWA">Rwanda</option>
						<option value="BLM">Saint Barthélemy</option>
						<option value="SHN">Saint Helena, Ascension and Tristan da Cunha</option>
						<option value="KNA">Saint Kitts and Nevis</option>
						<option value="LCA">Saint Lucia</option>
						<option value="MAF">Saint Martin (French part)</option>
						<option value="SPM">Saint Pierre and Miquelon</option>
						<option value="VCT">Saint Vincent and the Grenadines</option>
						<option value="WSM">Samoa</option>
						<option value="SMR">San Marino</option>
						<option value="STP">Sao Tome and Principe</option>
						<option value="SAU">Saudi Arabia</option>
						<option value="SEN">Senegal</option>
						<option value="SRB">Serbia</option>
						<option value="SYC">Seychelles</option>
						<option value="SLE">Sierra Leone</option>
						<option value="SGP">Singapore</option>
						<option value="SXM">Sint Maarten (Dutch part)</option>
						<option value="SVK">Slovakia</option>
						<option value="SVN">Slovenia</option>
						<option value="SLB">Solomon Islands</option>
						<option value="SOM">Somalia</option>
						<option value="ZAF">South Africa</option>
						<option value="SGS">South Georgia and the South Sandwich Islands</option>
						<option value="SSD">South Sudan</option>
						<option value="ESP">Spain</option>
						<option value="LKA">Sri Lanka</option>
						<option value="SDN">Sudan</option>
						<option value="SUR">Suriname</option>
						<option value="SJM">Svalbard and Jan Mayen</option>
						<option value="SWZ">Swaziland</option>
						<option value="SWE">Sweden</option>
						<option value="CHE">Switzerland</option>
						<option value="SYR">Syrian Arab Republic</option>
						<option value="TWN">Taiwan, Province of China</option>
						<option value="TJK">Tajikistan</option>
						<option value="TZA">Tanzania, United Republic of</option>
						<option value="THA">Thailand</option>
						<option value="TLS">Timor-Leste</option>
						<option value="TGO">Togo</option>
						<option value="TKL">Tokelau</option>
						<option value="TON">Tonga</option>
						<option value="TTO">Trinidad and Tobago</option>
						<option value="TUN">Tunisia</option>
						<option value="TUR">Turkey</option>
						<option value="TKM">Turkmenistan</option>
						<option value="TCA">Turks and Caicos Islands</option>
						<option value="TUV">Tuvalu</option>
						<option value="UGA">Uganda</option>
						<option value="UKR">Ukraine</option>
						<option value="ARE">United Arab Emirates</option>
						<option value="GBR">United Kingdom</option>
						<option value="USA">United States</option>
						<option value="UMI">United States Minor Outlying Islands</option>
						<option value="URY">Uruguay</option>
						<option value="UZB">Uzbekistan</option>
						<option value="VUT">Vanuatu</option>
						<option value="VEN">Venezuela, Bolivarian Republic of</option>
						<option value="VNM">Viet Nam</option>
						<option value="VGB">Virgin Islands, British</option>
						<option value="VIR">Virgin Islands, U.S.</option>
						<option value="WLF">Wallis and Futuna</option>
						<option value="ESH">Western Sahara</option>
						<option value="YEM">Yemen</option>
						<option value="ZMB">Zambia</option>
						<option value="ZWE">Zimbabwe</option>
					</select>						
				</div>								
			</div>
			<br class="clear" />			
		</div>

		<div class="control-group well clear pull-left span8">
			<div class="pull-left">
				<h3>Employment Details</h3> <hr />
				<div class="controls">
					<label for="company_name">Company Name</label>
					{{ Form::text('company_name', Input::old('company_name'), array('id' => 'company_name', 'class' => 'input-xlarge', 'placeholder' => 'company name') ) }}					
				</div>								
				<div class="controls">
					<label for="job_scope">Job Scope</label>
					{{ Form::textarea('job_scope', Input::old('job_scope'), array('id' => 'job_scope', 'class' => 'input-xlarge', 'cols' => '90', 'rows' => '5', 'placeholder' => 'Job Scope') ) }}					
				</div>
				<div class="controls">
					<label for="employment_date">Employment Date</label>
					<div class="input-append date" data-date="{{ date("Y-m-d") }}" data-date-format="yyyy-mm-dd">
					  <input name="employment_date" class="input-xlarge" size="16" type="text" value="{{ date("Y-m-d") }}">
					  <span class="add-on"><i class="icon-th"></i></span>
					</div>				      					
				</div>
				<div class="controls">
					<label for="salary">Salary</label>
					{{ Form::text('salary', Input::old('salary'), array('id' => 'salary', 'class' => 'input-xlarge', 'placeholder' => 'Monthly Salary') ) }}
				</div>
				<div class="controls">
					<label for="currency">Currency</label>
					{{ Form::text('currency', Input::old('currency'), array('id' => 'currency', 'class' => 'input-xlarge', 'placeholder' => 'Currency') ) }}
				</div>		
				<div class="controls">
					<label for="bank">Bank Name</label>
					{{ Form::text('bank', Input::old('bank'), array('id' => 'bank', 'class' => 'input-xlarge', 'placeholder' => 'Bank Name') ) }}					
				</div>
				<div class="controls">
					<label for="bank_branch">Bank Branch</label>
					{{ Form::text('bank_branch', Input::old('bank_branch'), array('id' => 'bank_branch', 'class' => 'input-xlarge', 'placeholder' => 'Bank branch') ) }}
				</div>						
				<div class="controls">
					<label for="account_name">Account Name</label>
					{{ Form::text('account_name', Input::old('account_name'), array('id' => 'account_name', 'class' => 'input-xlarge', 'placeholder' => 'Account Name') ) }}
				</div>
				<div class="controls">
					<label for="account_number">Account Number</label>
					{{ Form::text('account_number', Input::old('account_number'), array('id' => 'account_number', 'class' => 'input-xlarge', 'placeholder' => 'Account number') ) }}					
				</div>
				<div class="controls">
					<label for="account_type">Account Type</label>
					{{ Form::text('account_type', Input::old('account_type'), array('id' => 'account_type', 'class' => 'input-xlarge', 'placeholder' => 'Account type') ) }}
				</div>
				<div class="controls">
					<label for="others">Other Notes</label>
					{{ Form::textarea('others', Input::old('others'), array('id' => 'others', 'class' => 'input-xlarge', 'placeholder' => 'Other notes') )}}
				</div>				

				<div id="create_role" class="controls">
					<label for="id_role">Role</label>
					<select name="id_role" id="id_role">
						@foreach ($roles as $role)
						    <option value="{{ $role['id_role'] }}">{{ $role['role'] }}</option>
						@endforeach
					</select>
				</div>

				<div class="controls">
					<div class="controls btn-group">
						<label>Status</label>
						<div class="btn">
							{{ Form::label('status_on', 'Enable') }} {{ Form::radio('status', 1, false, array('id' => 'status_on')) }}
						</div>
						<div class="btn">
							{{ Form::label('status_off', 'Disable') }} {{ Form::radio('status', 0, true, array('id' => 'status_off')) }}
						</div>	
					</div>
				</div>										
			</div>

			<div class="pull-right span4">
				<h3>Employee Leave Details</h3> <hr />
				<div class="controls">
					<label for="annual_leave">Annual Leaves</label>
					{{ Form::text('annual_leave', Input::old('annual_leave'), array('id' => 'annual_leave', 'class' => 'input-xlarge', 'placeholder' => '14') ) }}
				</div>
				<div class="controls">
					<label for="sick_leave">Sick / Medical </label>
					{{ Form::text('sick_leave', Input::old('sick_leave'), array('id' => 'sick_leave', 'class' => 'input-xlarge', 'placeholder' => '6') ) }}
				</div>
				<div class="controls">
					<label for="maternity_leave">Maternity Leaves</label>
					{{ Form::text('maternity_leave', Input::old('maternity_leave'), array('id' => 'maternity_leave', 'class' => 'input-xlarge', 'placeholder' => '30') ) }}					
				</div>
				<div class="controls">
					<label for="paternity_leave">Paternity Leaves</label>
					{{ Form::text('paternity_leave', Input::old('paternity_leave'), array('id' => 'paternity_leave', 'class' => 'input-xlarge', 'placeholder' => '1') ) }}					
				</div>							
			</div>
			<br class="clear" />
		</div>

		<div class="control-group submit_button clear span8">
			<button class="btn btn-primary input-xlarge" id="user_create">Create a User</button>
		</div>
		{{ Form::hidden('department_name', '-') }}
		{{ Form::hidden('department_role_name', '-') }}
		
		{{ Form::close() }}
@stop