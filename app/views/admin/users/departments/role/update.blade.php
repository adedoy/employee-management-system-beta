@extends('admin.tpl.master')

@section('title')
  Update a User Department - Employee Management and Leave System
@stop

@section('content')

		<div class="page-header">
			<h1>Update a Department Role</h1>
			<p>update a department role.</p>
		</div>


        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('name', '<li>:message</li>') }}
          </ul> 
        @endif 

        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          <p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif        

        {{ Form::open() }}

		<div class="control-group">
			<label for="id_department" class="control-label">Department</label>
			<div class="controls">
				<select name="id_department" id="id_department">
					@foreach ( $departments as $department )
						<option value="{{ $department['id_department'] }}"@if ( $department['id_department'] == $selected_dept->id_department ) selected="selected"@endif>{{ $department['name'] }}</option>
					@endforeach
				</select>
			</div>
		</div>

		<div class="control-group">
			<label for="name" class="control-label">Department role name <sup class="text-error">*</sup></label>
			<div class="controls">
				{{ Form::text('name', isset($records->name) ? $records->name : '', array('id' => 'name', 'class' => 'input-xlarge', 'placeholder' => 'Department name')) }}
			</div>
		</div>		

		<div class="control-group">
			<label for="name" class="control-label">Department Role Description</label>
			<div class="controls">
				{{ Form::textarea('description', isset($records->description) ? $records->description : '', array('id' => 'description', 'class' => 'input-xlarge', 'placeholder' => 'Department description')) }}
			</div>
		</div>		

		<div class="control-group submit_button">
			<button class="btn btn-primary input-xlarge" id="department_role_update">Update</button>
		</div>

		{{ Form::close() }}
@stop