@extends('admin.tpl.master')

@section('title')
  User Department Roles - Employee Management and Leave System
@stop

@section('content')

		<div class="page-header">
			<h1>User Department Roles</h1>
			<p>departments roles.</p>
		</div>
        
        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          <p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif
		
		@if ( isset($roles) && count($roles) > 0 )
		<table class="table table-striped departments">
			<thead>
				<th>#</th>
				<th>Department</th>
				<th>Department Role</th>
				<th>Description</th>
				<th>Actions</th>
			</thead>
			<tbody>
				@foreach ( $roles as $role )
				<tr>
					<td> {{ $role->id_department_role}} </td>
					<td>{{ Departments::find($role->id_department)->name }}</td>
					<td> {{ $role->name}} </td>
					<td> {{ $role->description}} </td>					
					<td>
						<a href="{{ URL::to("admin/users/departments/role/update/$role->id_department_role") }}" class="btn btn-primary"><i class="icon-pencil icon-white"></i></a>
						<a href="{{ URL::to("admin/users/departments/role/delete/$role->id_department_role") }}" class="btn btn-danger"><i class="icon-remove icon-white"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
			<p>Sorry, no data to show.</p>
		@endif

		<br /><a href="{{ URL::to('admin/users/departments/role/create') }}" class="btn btn-primary">Add a department role</a>		
@stop