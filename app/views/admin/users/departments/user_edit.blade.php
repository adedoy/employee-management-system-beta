@extends('admin.tpl.master')

@section('title')
  Edit user to department - Employee Management and Leave System
@stop

@section('content')

		<div class="page-header">
			<h1>Edit user to department</h1>
			<p>edit user to department</p>
		</div>


        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('name', '<li>:message</li>') }}
          </ul> 
        @endif 

        {{ Form::open() }}

		<div class="control-group">
			<label for="id_user" class="control-label">Name</label>
			<div class="controls">
				{{ $users->last_name }}, {{ $users->first_name }}
			</div>
		</div>

		<div class="control-group">
			<label class="control-label">Department </label>
			<div class="controls">
				{{ $departments->name }}				
			</div>
		</div>		
		
		<div class="control-group">
			<label for="id_department_role" class="control-label">Department role</label>
			<div class="controls">
				<select name="id_department_role" id="id_department_role" class="input-xlarge">						
					@foreach ( $department_roles as $department_role)
						<option value="{{ $department_role->id_department_role }}"@if( $department_role->id_department_role == Request::segment(7)) selected="selected"@endif>{{ $department_role->name }}</option>
					@endforeach
				</select>				
			</div>
		</div>

		<div class="control-group submit_button">
			<button class="btn btn-primary input-xlarge" id="department_user_add">Edit user</button>
		</div>

		{{ Form::close() }}
@stop