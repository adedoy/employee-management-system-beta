@extends('admin.tpl.master')

@section('title')
  Delete a User Department - Employee Management and Leave System
@stop

@section('content')

		<div class="page-header">
			<h1>Delete a User Department</h1>
			<p>delete a user department.</p>
		</div>


        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>
          <ul class="alert alert-error">
              {{ $errors->first('name', '<li>:message</li>') }}
          </ul> 
        @endif 

        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          <p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif        

        {{ Form::open() }}

		<div class="control-group">
			<label for="name" class="control-label">Department Name <sup class="text-error">*</sup></label>
			<div class="controls">
				{{ Form::text('name', isset($records->name) ? $records->name : '', array('id' => 'name', 'class' => 'input-xlarge', 'placeholder' => 'Department name')) }}
			</div>
		</div>

		<div class="control-group">
			<label for="name" class="control-label">Department Description</label>
			<div class="controls">
				{{ Form::textarea('description', isset($records->description) ? $records->description : '', array('id' => 'description', 'class' => 'input-xlarge', 'placeholder' => 'Department description')) }}
			</div>
		</div>		

		<div class="control-group submit_button">
			<button class="btn btn-danger input-xlarge" id="department_delete" onClick="return confirm('Are you sure you want to delete this department?');">Delete this Department</button>
		</div>

		{{ Form::close() }}
@stop