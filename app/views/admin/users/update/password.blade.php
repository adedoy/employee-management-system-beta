@extends('admin.tpl.master')

@section('title')
  Users Update Password - Employee Management and Leave System
@stop

@section('content')
	
	<div class="page-header">
		<h1>Users Update Password</h1>
		<p>You will be prompted to login again after changing password.</p>
	</div>

	{{-- START THE UPDATE SECTION --}}
	
	{{ Form::open() }}

        @if ($errors->count() > 0)
         <p>The following errors have occurred:</p>

          <ul class="alert alert-error">
              {{ $errors->first('password', '<li>:message</li>') }}
          </ul> 
        @endif

        @if (Session::has('error'))
            <p class="alert alert-error"> {{ Session::get('error') }} </p>
        @elseif ( Session::has('success') )
          	<p class="alert alert-success"> {{ Session::get('success') }} </p>
        @endif

		<div class="control-group well">
			<div class="controls">
				<label>Type in you password</label>
				<input type="password" name="password" id="password" value="" />
				<span class="help-inline">*Must be at least 8 characters</span>
			</div>			
		</div>		

		<div class="control-group submit_button">
			<a href="{{ URL::to('admin/users/update') }}/{{ $records->id_user }}" class="btn btn-inverse">Cancel</a>
			<input type="submit" class="btn btn-primary input-xlarge" name="user_update_password" id="user_update_password" value="Update Password">
		</div>
	{{ Form::close() }}	
@stop